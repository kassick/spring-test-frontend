package org.kzk.springtest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Pair
{
    public Pair(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public Pair() {
        this("<NONE>", "<NONE>");
    }



    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private String key;
    private String value;
}
