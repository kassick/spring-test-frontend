package org.kzk.springtest;

public class KeyItem
{
    public String key;
    public String keyPath;

    public KeyItem(String key, String keyPath) {
        this.key = key;
        this.keyPath = keyPath;
    }
}
