package org.kzk.springtest;

import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.stream.Collectors;

@Controller
public class PairController
{

    @GetMapping("/")
    public String getRootPage( Model model,
                               RestTemplate restTemplate)
    {
        String[] keys = restTemplate.getForObject(
                "http://localhost:10000/key/",
                String[].class);

        model.addAttribute("keys",
                Arrays.stream(keys)
                        .map(k -> new KeyItem(k, "keys/" + k))
                        .collect(Collectors.toList()));

        return "interactForm" ;
    }

    @PostMapping("/search")
    public String getSearchResults( Model model,
                                    RestTemplate restTemplate,
                                    @RequestParam("searchKey") String text
                                   )
    {

        String[] keys;

        if (text != null && text.length() > 0)
            keys = restTemplate.getForObject(
                "http://localhost:10000/searchKeys?text={text}",
                String[].class,
                text);
        else
            keys = restTemplate.getForObject(
                    "http://localhost:10000/key/",
                    String[].class);

        model.addAttribute("searchKey", text);
        model.addAttribute("keys",
                           Arrays.stream(keys)
                                   .map(k -> new KeyItem(k, "keys/" + k))
                                   .collect(Collectors.toList()));

        return "interactForm";
    }

    @PostMapping("/insert")
    public String insertNewPair(Model model,
                                RestTemplate restTemplate,
                                @RequestParam("insertKey") String key,
                                @RequestParam("insertValue") String value)
    {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("value", value);
        Pair p = restTemplate.postForObject("http://localhost:10000/key/{key}", params, Pair.class, key);

        model.addAttribute("message", String.format("Inserted %s -> %s", key, value));

        String[] keys = restTemplate.getForObject(
                "http://localhost:10000/key/",
                String[].class);

        model.addAttribute("keys",
                Arrays.stream(keys)
                        .map(k -> new KeyItem(k, "keys/" + k))
                        .collect(Collectors.toList()));

        return "interactForm";
    }

    @GetMapping("/keys/{key:.+}")
    public String getOnePair(Model model,
                             RestTemplate restTemplate,
                             @PathVariable("key") String key)
    {
        Pair p = restTemplate.getForObject(
                "http://localhost:10000/key/{key}",
                Pair.class,
                key);

        model.addAttribute("key", p.getKey());
        model.addAttribute("value", p.getValue());

        return "kvForm";
    }

    @PostMapping("/keys/{key:.+}")
    public String updateOnePair(Model model,
                                RestTemplate restTemplate,
                                @PathVariable("key") String key,
                                @RequestParam("updateValue") String value)
    {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("value", value);
        Pair p = restTemplate.postForObject(
                    "http://localhost:10000/key/{key}",
                    params,
                    Pair.class,
                    key);

        model.addAttribute(
                "message",
                String.format("Updated value: %s -> %s", p.getValue(), value));

        model.addAttribute("key", key);
        model.addAttribute("value", value);
        return "kvForm";
    }

}
